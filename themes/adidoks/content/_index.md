+++
title = "Zhengqi Wang"


# The homepage contents
[extra]
lead = '<b>Welcome</b> to my home page!'
url = "/docs/getting-started/introduction/"
url_button = "Home Page"

# Menu items
[[extra.menu.main]]
name = "PERSONAL BIOGRAPHY"
section = "docs"
url = "/docs/getting-started/introduction/"
weight = 10

[[extra.menu.main]]
name = "Blog"
section = "blog"
url = "/blog/"
weight = 20

[[extra.list]]
title = "LinkedIn"
#content = 'Get A+ scores on <a href="https://observatory.mozilla.org/analyze/adidoks.org">Mozilla Observatory</a> out of the box. Easily change the default Security Headers to suit your needs.'
content = 'A detailed personal biography on <br> <a href="https://www.linkedin.com/in/zhengqi-wang---/">LinkedIn Homepage</a> '

[[extra.list]]
title = "Fans of"
content = '<a href="https://www.ferrari.com/en-EN/formula1/team">Tifosi</a>🏎️ <br> <a href="https://www.fcbarcelona.com/en/">FC Barcelona</a>  🔵🔴'



[[extra.list]]
title = "GitHub"
content = 'My Github home page on <br> <a href="https://github.com/supergeorge23">GitHub</a> '

[[extra.list]]
title = "Education Background"
content = "Current graduate student specializing in Electrical and Computer Engineering at Duke University, obtained a honor Bachelor degree in Financial Mathematics from University of Liverpool."

[[extra.list]]
title = "Elegant Coder and Discerning Trader"
content = "My goal is to contribute to a professional network where collective effort leads to significant advancements and practical solutions in our industry."

[[extra.list]]
title = "More Hobbies"
content = "Fitness Fanatic, Cooking Enthusiast, Loves Puppies"
+++
