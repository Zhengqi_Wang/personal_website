# Personal Website with Continuous Delivery

This project demonstrates how to create a personal website using Zola, automate the build and deployment process using GitLab CI/CD, and host the website on Vercel.

## Prerequisites

- GitLab account
- Vercel account
- Basic understanding of Git

## Setting Up Your Project

### Step 1: Create a New Zola Site

1. Install Zola on your local machine. Follow the installation guide here: [https://www.getzola.org/documentation/getting-started/installation/](https://www.getzola.org/documentation/getting-started/installation/)
2. Create a new Zola site by running `zola init mysite`.
3. Navigate into your site directory `cd mysite`.

### Step 2: Initialize a Git Repository

1. Initialize a new Git repository with `git init`.
2. Add your Zola site files to the repository using `git add .` and commit them with `git commit -m "Initial Zola site setup"`.

### Step 3: Push to GitLab

1. Create a new repository on GitLab.
2. Add the GitLab remote to your local repository: `git remote add origin <YOUR_GITLAB_REPO_URL>`.
3. Push your code to GitLab with `git push -u origin master`.

## Setting Up Continuous Delivery

### Step 1: Configure GitLab CI/CD

1. Create a `.gitlab-ci.yml` file in project root.
2. Add the following configuration to build and deploy site on each push:

```yaml
stages:
  - deploy

default:
  image: debian:stable-slim
  tags:
    - docker

variables:
  # The runner will be able to pull your Zola theme when the strategy is
  # set to "recursive".
  GIT_SUBMODULE_STRATEGY: "recursive"

  # If you don't set a version here, your site will be built with the latest
  # version of Zola available in GitHub releases.
  # Use the semver (x.y.z) format to specify a version. For example: "0.17.2" or "0.18.0".
  ZOLA_VERSION:
    description: "The version of Zola used to build the site."
    value: ""

pages:
  stage: deploy
  script:
    - |
      apt-get update --assume-yes && apt-get install --assume-yes --no-install-recommends wget ca-certificates
      if [ $ZOLA_VERSION ]; then
        zola_url="https://github.com/getzola/zola/releases/download/v$ZOLA_VERSION/zola-v$ZOLA_VERSION-x86_64-unknown-linux-gnu.tar.gz"
        if ! wget --quiet --spider $zola_url; then
          echo "A Zola release with the specified version could not be found.";
          exit 1;
        fi
      else
        github_api_url="https://api.github.com/repos/getzola/zola/releases/latest"
        zola_url=$(
          wget --output-document - $github_api_url |
          grep "browser_download_url.*linux-gnu.tar.gz" |
          cut --delimiter : --fields 2,3 |
          tr --delete "\" "
        )
      fi
      wget $zola_url
      tar -xzf *.tar.gz
      ./zola build

  artifacts:
    paths:
      # This is the directory whose contents will be deployed to the GitLab Pages
      # server.
      # GitLab Pages expects a directory with this name by default.
      - .

  rules:
    # This rule makes it so that your website is published and updated only when
    # you push to the default branch of your repository (e.g. "master" or "main").
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```
This configuration installs Zola in an Alpine Linux environment, builds own site, and sets the public directory as an artifact.
![Pipeline](Images/CI_CD.png)


### Step 2: Set Up Vercel Deployment
1. Log in to Vercel account and create a new project.
2. Link GitLab repository to Vercel.
3. Configure Vercel to deploy the public directory.
4. Now, every time push changes to your repository, GitLab CI/CD will automatically build your site, and Vercel will deploy the updated version.


## Host on Vercel
1. Connect Gitlab repo towards vercel, and website can we view on:
![Vercel Screenshoot](Images/Vercel_ScreenShoot.png)

### Live Website

Visit the live version of the personal website here: [https://personal-website-wine-rho.vercel.app/](https://personal-website-wine-rho.vercel.app/)


### Demo Video
[View Description Video](descriptions.mp4)


