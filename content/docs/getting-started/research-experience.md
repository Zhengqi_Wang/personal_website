+++
title = "Research Experience"
description = "One page summary of how to start a new AdiDoks project."
date = 2021-05-01T08:20:00+00:00
updated = 2021-05-01T08:20:00+00:00
draft = false
weight = 20
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "One page summary of my former research experience."
toc = true
top = false
+++

## Final Year Project Research: Decomposition of Noised Signal (Python)

Conducted comprehensive research utilizing Singular Spectrum Analysis (SSA) and its derivatives for signal decomposition and noise reduction. Innovated Adaptive-TSVD and introduced Slid-SSA to enhance computational efficiency. Employed Sparse Autoencoder - Radial Basis Function Networks, and provided critical insights into the decomposition outcomes of large time signal dataset. The findings contributed to the academic understanding of signal processing and offered innovative approaches to the examination of periodic features within complex time series.

## Quantitative Measurements of Neural Signal Synchronization (MATLAB)

Implemented advanced neural signal processing techniques, utilizing a non-linear time series approach with singular spectrum analysis, enhanced by ISI- and SPIKE-Distance to reveal local characteristics. Introduced Non-Linear Interdependence (NLI) methods to quantify neural synchronization across brain regions, applied parameter-free measures for spike train synchrony, and innovatively modified and repaired SPIKE-Distance methodology, enabling precise monitoring of time-resolved synchrony in continuous data, reflecting a substantial contribution to the field.

## Machine Learning Project: Compressed Sensing Image Recovery 
Participated in a project guided by Stacy L. Tantum, Ph.D. ([Duke University Professor](https://ece.duke.edu/faculty/stacy-tantum)), focused on compressed sensing techniques to recover full images from a small number of sampled pixels. The project achieved sparse reconstruction of images by applying regularized regression (LASSO) on DCT coefficients. Responsibilities included constructing the basis vector matrix, selecting regularization parameters, implementing cross-validation, and employing post-processing techniques (such as median filtering) to optimize the quality of the recovered images, thereby improving the accuracy of image recovery by reducing the Mean Squared Error (MSE).
<img alt="Compressed Sensing Image Recovery Results" src="/Users/supergeorge/adidoks/MP1/Photo/TXT_Image_Reconstruction.png"/>


<button onclick="window.history.back();">Back</button>